### STAGE 1: Build ###
FROM node:16.15.1-alpine AS build
WORKDIR /usr/src/app
COPY package.json ./
#RUN npm i g @angular/cli@13.2.4
#RUN ng --version
RUN npm install
COPY . .
RUN node_modules/.bin/ng build --configuration="production"


### STAGE 2: Run ###
FROM nginx:1.22-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist/ris_frontend /usr/share/nginx/html
# COPY .htaccess /usr/share/nginx/html
#CMD ["nginx", "-g", "daemon off;"]

EXPOSE 80
