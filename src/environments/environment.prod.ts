export const environment = {
  production: true,
  baseUrl: 'http://visamigration.eastus.cloudapp.azure.com:8090/api/auth',
  empUrl: 'http://visamigration.eastus.cloudapp.azure.com:8092',
  visaUrl: 'http://visamigration.eastus.cloudapp.azure.com:8093',
  reportUrl: 'http://visamigration.eastus.cloudapp.azure.com:8094/api/excel',
  regiUrl: 'http://visamigration.eastus.cloudapp.azure.com:8091',
};
