import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Documents } from '../_classes/documents';
import { IdleService } from '../_services/idle.service';
import { StorageService } from '../_services/storage.service';
import { VisaService } from '../_services/visa.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {
  form!: FormGroup;
  submitted = false;
  loading = false;

  private docDetail = new Documents();
  status: boolean | undefined;
    doc_documentTitle: string| undefined;
    doc_issuingAuthority: string| undefined;
    doc_documentNumber: string| undefined;
    doc_expDate: any| undefined;
    doc_additionalInfo: string| undefined;
    userId: string | undefined;
    user_id!: number;
    user_role!: number;

  constructor(private router: Router, 
    private visaService: VisaService,
    private idleService: IdleService,
    private storageService: StorageService,
    private formBuilder: FormBuilder,
	private datePipe: DatePipe) { }

  ngOnInit(): void {

    this.user_id = this.storageService.secureStorage.getItem("userId");
    this.user_role = this.storageService.secureStorage.getItem("role");
    this.visaService.getDocDetails(this.user_id).subscribe(
      (response) => {
        console.log(response)  
        this.doc_documentTitle = response.documentTitle;
        this.doc_issuingAuthority = response.issuingAuthority;
        this.doc_documentNumber = response.documentNumber;
        this.doc_expDate = this.datePipe.transform(response.expDate,'yyyy-MM-dd');
        this.doc_additionalInfo = response.additionalInfo;
      },
      (error) => {
         if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          
        }
      }
    );

    this.form = this.formBuilder.group(
      {
        documentTitle: ['', Validators.required],
        issuingAuthority: ['', Validators.required],
        documentNumber: ['', Validators.required, ],
        expDate: ['', Validators.required],
        additionalInfo: ['', Validators.required],
       }
    );
  }
  onSubmit(){
    this.submitted = true;
    if (this.form?.invalid) {
      return;
    }
    this.loading = true;
    console.log(JSON.stringify(this.form?.value, null, 2));
    this.docDetail.userId = this.storageService.secureStorage.getItem("userId");
    this.docDetail.documentTitle = this.documentTitle?.value;
    this.docDetail.issuingAuthority = this.issuingAuthority?.value;
    this.docDetail.documentNumber = this.documentNumber?.value;
    this.docDetail.expDate = this.expDate?.value;
    this.docDetail.additionalInfo = this.additionalInfo?.value;
    this.visaService.saveDocDetails(this.docDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        this.status = false;
        alert(
          response.body.message
        );
        this.loading = false;
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          alert(
            "Document Infomation not saved Successfully!"
          );
          this.loading = false;
        }
      }
    );
   
  }

  onReset() {
    this.submitted = false;
    this.form?.reset();
  }

  get documentTitle() {
    return this.form.get("documentTitle");
  }

  get issuingAuthority() {
    return this.form.get("issuingAuthority");
  }

  get documentNumber() {
    return this.form.get("documentNumber");
  }

  get expDate() {
    return this.form.get("expDate");
  }

  get additionalInfo() {
    return this.form.get("additionalInfo");
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form?.controls;
  }

}
