import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService } from '../_services/alert.service';
import { AdminService } from '../_services/admin.service';
import Validation from '../_utils/validation';
import { PasswordModel } from '../_classes/password';
import { UserService } from '../_services/user.service';
import { IdleService } from '../_services/idle.service';
import { StorageService } from '../_services/storage.service';

@Component({
  selector: 'app-change-user-password',
  templateUrl: './change-user-password.component.html',
  styleUrls: ['./change-user-password.component.css']
})
export class ChangeUserPasswordComponent implements OnInit {

  private passwordDetail = new PasswordModel();

  form!: FormGroup;
  loading = false;
  submitted = false;
    status!: boolean;
   

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private userService: UserService,
      private idleService: IdleService,
      private storageService: StorageService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.form = this.formBuilder.group({
        oldPassword:       ['', [Validators.required, 
          Validators.minLength(8), 
          Validators.maxLength(40),
          Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
        ]],
        newPassword:       ['', [Validators.required, 
          Validators.minLength(8), 
          Validators.maxLength(40),
          Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
        ]],
        confirmNewPassword:['', Validators.required]
      },
      {
          validators: [Validation.match('password', 'confirmPassword')]
        } 
      );

  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  onSubmit() {
      this.submitted = true;

      // reset alerts on submit
      this.alertService.clear();

      // stop here if form is invalid
      if (this.form.invalid) {
          return;
      }

      this.loading = true;
        this.passwordDetail.oldPassword = this.oldPassword?.value;
        this.passwordDetail.newPassword = this.newPassword?.value;
        this.passwordDetail.email = this.storageService.secureStorage.getItem("email");

      this.userService.changePassword(this.passwordDetail).subscribe(
        (response) => {
          this.idleService.checkUserIdleOrNot();
          this.status = false;
          this.loading = false;
          alert(
            "Password Changed Successfully !!"
          );
          this.router.navigate(["/login"]);
        },
        (error) => {
          this.status = false;
          if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
            this.loading = false;
            alert(
              "Invalid old password  !!"
            );
          }
        }
      );
  }

  onReset() {
    this.submitted = false;
    this.form?.reset();
  }

  get oldPassword() {
    return this.form.get("oldPassword");
  }

  get newPassword() {
    return this.form.get("newPassword");
  }

  get confirmNewPassword() {
    return this.form.get("confirmNewPassword");
  }
}