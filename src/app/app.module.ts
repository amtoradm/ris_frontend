import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { EmployeeComponent } from './employee/employee.component';
import { VisaComponent } from './visa/visa.component';
import { ReportComponent } from './report/report.component';
import { ReviewComponent } from './review/review.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { HomepageComponent } from './homepage/homepage.component';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { UsersComponent } from './users/users.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrganizationComponent } from './organization/organization.component';
import { AddressComponent } from './address/address.component';
import { DocumentsComponent } from './documents/documents.component';
import { DocumentsuploadComponent } from './documentsupload/documentsupload.component';
import { MMDDYYYYDatePipe } from './_pipes/mm-dd-yyyy-date.pipe';
import { DatePipe } from '@angular/common';
import { ChangeUserPasswordComponent } from './change-user-password/change-user-password.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    EmployeeComponent,
    VisaComponent,
    ReportComponent,
    ReviewComponent,
    UserprofileComponent,
    ForgetpasswordComponent,
    HomepageComponent,
    ChangepasswordComponent,
    UsersComponent,
    OrganizationComponent,
    AddressComponent,
    DocumentsComponent,
    DocumentsuploadComponent,
    MMDDYYYYDatePipe,
    ChangeUserPasswordComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    HttpClientModule,
    NgIdleKeepaliveModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 2000,
    }),
    ToastContainerModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
