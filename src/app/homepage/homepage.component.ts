import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../_services/employee.service';
import { StorageService } from '../_services/storage.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  user_role!: string;


  constructor(private storageService: StorageService,
    private empService: EmployeeService) { }

  isRegister:boolean = false;
  isUsers:boolean = false;
  isReports:boolean = false;
  isEmployee:boolean = false;
  isVisa:boolean = false;
  isReview:boolean = false;

  isEmployeeRole:boolean = false;
  isAdminRole:boolean = false;
  isHRRole:boolean = false;

  
  ngOnInit(): void {
    
    this.user_role = this.storageService.secureStorage.getItem("role");
    if(this.user_role == "employee"){
      this.isEmployeeRole = true;
      this.isEmployee=true;
    }
    if(this.user_role == "hr"){
      this.isHRRole = true;
      this.isRegister=true;
    }
    if(this.user_role == "admin" || this.user_role == ""){
      this.isAdminRole = true;
      this.isRegister=true;
    }
  }

  onRegister(){
    this.isRegister=true;
    this.isUsers = false;
    this.isReports = false;
    this.isEmployee = false;
    this.isVisa=false;
    this.isReview = false;
  }

  onUsers(){
    this.isRegister=false;
    this.isUsers = true;
    this.isReports = false;
    this.isEmployee = false;
    this.isVisa=false;
    this.isReview = false;
  }

  onReports(){
    this.isRegister=false;
    this.isUsers = false;
    this.isReports = true;
    this.isEmployee = false;
    this.isVisa=false;
    this.isReview = false;
  }

  onEmployee(){
    this.isRegister=false;
    this.isUsers = false;
    this.isReports = false;
    this.isEmployee = true;
    this.isVisa=false;
    this.isReview = false;
    this.empService.isEmpReview=false;
  }

  onVisa(){
    this.isRegister=false;
    this.isUsers = false;
    this.isReports = false;
    this.isEmployee = false;
    this.isVisa=true;
    this.isReview = false;
    this.empService.isEmpReview=false;
  }

  onReview(){
    this.isRegister=false;
    this.isUsers = false;
    this.isReports = false;
    this.isEmployee = false;
    this.isVisa=false;
    this.isReview = true;
  }

}
