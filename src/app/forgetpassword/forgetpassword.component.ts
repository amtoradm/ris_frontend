import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from '../_services/admin.service';
import { AlertService } from '../_services/alert.service';
import { first, finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { IdleService } from '../_services/idle.service';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {

  form!: FormGroup;
  loading = false;
  submitted = false;
  status: boolean = false;
    
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    private idleService: IdleService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
  });
  }

  // convenience getter for easy access to form fields
  get f(): { [key: string]: AbstractControl } {
    return this.form?.controls;
  }

  get email() {
    return this.form.get("email");
  }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    //stop here if form is invalid
    if (this.form.invalid) {
        return;
    }

    this.loading = true;
    this.alertService.clear();
    this.userService.checkAccountAndSentResetPasswordLink(this.email?.value).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        console.log(response);
        this.status = false;
   
           alert(
             "Password reset link has sent on your mail, Please check your mail"
           );
           this.loading = false;
        this.router.navigate(["/login"]);
      },
      (error) => {
        this.status = false;
        this.alertService.error("Username account is not available !!");
        this.loading = false;
        alert(
          "Username is not available !!"
        );
      });
}
}
