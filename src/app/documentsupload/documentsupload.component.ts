import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { FileuploadingService } from '../_services/fileuploading.service';
import { StorageService } from '../_services/storage.service';

import { UploadFileService } from '../_services/upload-file.service';

@Component({
  selector: 'app-documentsupload',
  templateUrl: './documentsupload.component.html',
  styleUrls: ['./documentsupload.component.css']
})
export class DocumentsuploadComponent implements OnInit {
//   selectedFiles!: FileList;
//   currentFile!: File;
//   progress = 0;
//   message = '';
//   fileInfos!: Observable<any>;

//   constructor(private uploadService: UploadFileService) { }

//   ngOnInit(): void {
//     this.fileInfos = this.uploadService.getFiles();
//     console.log("Files = "+this.fileInfos);
    
//   }

//   selectFile(event : any): void {
//     this.selectedFiles = event.target.files;
//     console.log(this.selectedFiles)
//   }

//   upload(): void {
//     this.progress = 0;
//     if (this.selectedFiles) {
//       const file: File | null = this.selectedFiles.item(0);
//       if (file) {
//         this.currentFile = file;
//         this.uploadService.upload(this.currentFile).subscribe(
//           (event: any) => {
//             if (event.type === HttpEventType.UploadProgress) {
//               this.progress = Math.round(100 * event.loaded / event.total);
//             } else if (event instanceof HttpResponse) {
//               this.message = event.body.message;
//               this.fileInfos = this.uploadService.getFiles();
//             }
//           },
//           (err: any) => {
//             console.log(err);
//             this.progress = 0;
//             if (err.error && err.error.message) {
//               this.message = err.error.message;
//             } else {
//               this.message = 'Could not upload the file!';
//             }
//             //this.currentFile = undefined;
//           });
//       }
//       //this.selectedFiles = undefined;
//     }
//   }

// }


selectedFiles?: FileList;
currentFile?: File;
progress = 0;
message = '';
userId: number | undefined;

fileInfos?: Observable<any>;
  email: any;

constructor(private fileuploadingService: FileuploadingService,
  private storageService: StorageService) { }
ngOnInit() {
  this.userId = this.storageService.secureStorage.getItem("userId");
  this.email = this.storageService.secureStorage.getItem("email");
  //this.fileInfos = this.fileuploadingService.getFiles();
}

selectFile(event: any) {
  this.progress = 0;
  this.selectedFiles = event.target.files;
}

upload(): void {
  this.progress = 0;

  if (this.selectedFiles) {
    const file: File | null = this.selectedFiles.item(0);

    if (file) {
      this.currentFile = file;

      this.fileuploadingService.upload(this.currentFile,this.userId, this.email).subscribe(
        (event: any) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            this.message = event.body.message;
            this.fileInfos = this.fileuploadingService.getFiles();
          }
        },
        (err: any) => {
          console.log(err);
          this.progress = 0;

          if (err.error && err.error.message) {
            this.message = err.error.message;
          } else {
            this.message = 'Could not upload the file!';
          }

          this.currentFile = undefined;
        });
    }

    this.selectedFiles = undefined;
  }
}
}
