export class User {
    firstName!: string;
	lastName!: string;
	email!: string;
    password!: string;
     roles!: string[];
    token!: string;
    enabled!: string;
}