export class Documents {
	userId!: number;
    documentTitle!: string;
	issuingAuthority!: string;
	documentNumber!: string;
	expDate!: Date;
    additionalInfo!: string;
}