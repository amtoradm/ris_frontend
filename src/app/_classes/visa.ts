export class Visa {
    userId!: number;
    isUSCitizen!: string;
    isNonUSCitizen!: string;
    isEmplawfulPermanentResident!: string;
    isAuthorizedToWork!: string;
    permAlienRegNo!: string;
    alienAuthToWorkRegNo!: string;
    alienAuthToWorkI94AdmiNo!: string;
    alienAuthToWorkForeignPassportNum!: string;
    alienAuthToWorkForeignPassportCountry!: string;
    authWorkExpDate!: Date;
    docTitle!: string;
    docNumber!: string;
    docExpDate!: Date;
    visaCategory!: string;
    visaEarliestRenewalDate!: Date;
    visaLatestRenewalDate!: Date;
    visaExpDate!: Date
 }