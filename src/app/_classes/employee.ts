export class Employee {
    userId!: number;
    firstName!: string;
	lastName!: string;
	middleInitial!: string;
	otherLastName!: string;
    dateOfBirth!: string;
    gender!: string[];
    ssn!: number;
    email!: string;
    telephoneNumber!: number;
    mobileNumber!: number;
    alternateMobileNumber!: number;
}