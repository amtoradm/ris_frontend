export class AdminDetail {
    emailId!:   string;
	email!:     string;
	password!:  string;
	role!:      string;
	id!:        number;
    username!:  string;
    //roles!:     string[];
    token!:     string;
}