export class Address {
    userId!: number;
    aptNumber!: string;
	sreetNumberAndName!: string;
	pinCodeOrZipCode!: number;
	areaLocality!: string;
    townCity!: string;
    state!: string[];
    country!: string;
}