export class Organization {
    userId!: number;
    lastNameOfEmpOrAuthorizedRepresentative!: string;
	firstNameOfEmpOrAuthorizedRepresentative!: string;
	empBusinessOrOrgName!: string;
	titleOfEmpOrAuthorizedRepresentative!: string;
    firstDayOfEmployment!: string;
    dateofRehire!: string;
    streetNumberAndName!: string;
	pinCodeOrZipCode!: number;
    townCity!: string;
    state!: string[];
    country!: string;
    salary!: string;
	clientName!: string;
	employmentLocation!: string;
	projectEndDate!: Date;      
}