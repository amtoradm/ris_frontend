export class PasswordModel {
	email!: string;
    password!: string;
    token!: string;
    oldPassword!: string;
    newPassword!: string;
}