import { Component, OnInit } from '@angular/core';
import { ReportService } from '../_services/report.service';
import { StorageService } from '../_services/storage.service';
import * as XLSX from 'xlsx'; 

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  user_role: string | undefined;
  user_id: string | undefined;
  listOfUser: any;
  status: boolean | undefined;
  searchedKeyword!: string;

  isEmployeeRole:boolean = false;
  isAdminRole:boolean = false;
  isHRRole:boolean = false;

  excelFileName= "ris_report" + "_" + Math.round(new Date().getTime()/1000)+".xlsx";

  constructor(    private storageService: StorageService,
    private reportService: ReportService
    ) { }

  ngOnInit(): void {
    this.user_id = this.storageService.secureStorage.getItem("userId");
    this.user_role = this.storageService.secureStorage.getItem("role");

    if(this.user_role == "employee"){
      this.isEmployeeRole = true;
      this.getEmployeeReportDetails(this.user_id);
    }
    if(this.user_role == "hr"){
      this.isHRRole = true;
      this.getAllEmployeeDetails();
    }
    if(this.user_role == "admin" || this.user_role == ""){
      this.isAdminRole = true;
      this.getAllEmployeeDetails();
    }


    
  }

  

  isViewReport:boolean = false;

  viewReport(){
    this.isViewReport = true;
  }

  getAllEmployeeDetails(){
    this.reportService.getEmpReport().subscribe(
      (response) => {  
        this.listOfUser = response;
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          
        }
      }
    );
  }

  getEmployeeReportDetails(userId :any){
    this.reportService.getEmpReportDetails(userId).subscribe(
      (response) => {  
        this.listOfUser = response;
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          
        }
      }
    );
  }

//   filename!: string;
//   downloadExcel(){
//     this.reportService.downloadExcelReport().subscribe(
//     (response: any) =>{
//       this.filename = 'ris_report.xlsx';
//         let dataType = response.type;
//         let binaryData = [];
//         binaryData.push(response);
//         let downloadLink = document.createElement('a');
//         downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
//         if (this.filename)
//             downloadLink.setAttribute('download', this.filename);
//         document.body.appendChild(downloadLink);
//         downloadLink.click();
//     }
// )
// }

downloadExcel(): void 
    {
      //this.service_obj.activityLogs("Locked Order #",this.userId,this.user_country_name);

       /* table id is passed over here */   
       let element = document.getElementById('excel-table'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

       /* save to file */
       XLSX.writeFile(wb, this.excelFileName);
			
    }

  downLoadFile(data: any, type: string) {
    let blob = new Blob([data], { type: type});
    let url = window.URL.createObjectURL(blob);
    let pwa = window.open(url);
    if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
        alert( 'Please disable your Pop-up blocker and try again.');
    }
}
}
