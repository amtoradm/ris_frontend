import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, from } from "rxjs";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  private reportUrl = environment.reportUrl;
  constructor(
    private http: HttpClient
  ) { }

  getEmpReport(): Observable<any> {
    let url =  this.reportUrl +"/getEmployee";
    return this.http.get(`${url}`);
  }

  getEmpReportDetails(userId: any): Observable<any> {
    let url =  this.reportUrl +"/getEmployeeReportDetails?userId";
    return this.http.get(`${url}=${userId}`);
  }

  downloadExcelReport(): Observable<any> {
    let url =  this.reportUrl +"/download";
    console.log(url);
     return this.http.get(`${url}`,{responseType: 'blob' as 'json'});
  }

  

}
