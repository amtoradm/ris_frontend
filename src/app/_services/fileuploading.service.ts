import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class FileuploadingService {

  private visaUrl = environment.visaUrl;

  constructor(private http: HttpClient) { }

  upload(file: File, userId:any, email:any): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);
    formData.append('userId',userId);
    formData.append('email',email);

    const req = new HttpRequest('POST', `${this.visaUrl}/upload`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }

  getFiles(): Observable<any> {
    return this.http.get(`${this.visaUrl}/files`);
  }
}


