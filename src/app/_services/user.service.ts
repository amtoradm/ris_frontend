import { Injectable } from '@angular/core';
import { StorageService } from "../_services/storage.service";
import { Observable, BehaviorSubject, from } from "rxjs";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { User } from '../_classes/user';
import { PasswordModel } from '../_classes/password';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private regiUrl = environment.regiUrl;

  constructor(private storageService: StorageService,
    private http: HttpClient,
    private toastr: ToastrService,
    private router: Router) { }

    register(userDetail: User): Observable<any> {
      let url =  this.regiUrl +"/register";
      return this.http.post(url, userDetail, { observe: "response" as "body" })
    }

    getUsersList(): Observable<any> {
      let url = this.regiUrl +"/getUser";
      return this.http.get(`${url}`);
    }

    deleteUser(userDetail: User): Observable<any> {
      let url = this.regiUrl +"/deleteUser";
      return this.http.post(url, userDetail, { observe: "response" as "body" })
    }

    EnableOrisableUser(userDetail: User): Observable<any> {
      let url = this.regiUrl +"/enableOrisableUser";
      return this.http.post(url, userDetail, { observe: "response" as "body" })
    }

    resetPassword(passwordDetail: PasswordModel): Observable<any> {
      let url = this.regiUrl +"/savePassword";
      return this.http.post(url, passwordDetail, { observe: "response" as "body" })
    }

    changePassword(passwordDetail: PasswordModel): Observable<any> {
      let url = this.regiUrl +"/changePassword";
      return this.http.post(url, passwordDetail, { observe: "response" as "body" })
    }

    checkAccountAndSentResetPasswordLink(email: any): Observable<any> {
      let url = this.regiUrl +"/checkAccountAndSentResetPasswordLink?email";
      return this.http.get(`${url}=${email}`);
    }

    verifyToken(token: any): Observable<any> {
      let url = this.regiUrl +"/verifyToken?token";
      return this.http.get(`${url}=${token}`);
    }

  }