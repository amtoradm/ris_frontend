import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {
  private visaUrl = environment.visaUrl;
  constructor(private http: HttpClient) { }
  
  upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    console.log("Form Data"+formData);
      const req = new HttpRequest('POST', `${this.visaUrl}/upload`, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    console.log("response"+this.http.request(req));
    return this.http.request(req);
  }

  getFiles(): Observable<any> {
    return this.http.get(`${this.visaUrl}/files`);
  }
}