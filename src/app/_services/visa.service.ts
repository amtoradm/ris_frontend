import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, from } from "rxjs";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { StorageService } from './storage.service';
import { Visa } from '../_classes/visa';
import { Documents } from '../_classes/documents';

@Injectable({
  providedIn: 'root'
})
export class VisaService {
  private visaUrl = environment.visaUrl;

  constructor(private storageService: StorageService,
    private http: HttpClient,
    private toastr: ToastrService,
    private router: Router) { }

    saveVisaDetails(visaDetail: Visa): Observable<any> {
      let url =  this.visaUrl +"/visa";
      console.log("VisaService");
      return this.http.post(url, visaDetail, { observe: "response" as "body" })
    }

    saveDocDetails(documentDetail: Documents): Observable<any> {
      let url = this.visaUrl +"/document";
      console.log("VisaService");
      return this.http.post(url, documentDetail, { observe: "response" as "body" })
    }

    getVisaDetails(email: any): Observable<any> {
      let url =this.visaUrl +"/getVisa?email";
      console.log("VisaServicen= "+url);
      return this.http.get(`${url}=${email}`);
    }

    getDocDetails(email: any): Observable<any> {
      let url = this.visaUrl +"/getDocument?email";
      console.log("VisaService = "+url);
      return this.http.get(`${url}=${email}`);
    }

  }