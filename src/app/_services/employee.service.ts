import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, from } from "rxjs";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { StorageService } from './storage.service';
import { Employee } from '../_classes/employee';
import { Organization } from '../_classes/organization';
import { Address } from '../_classes/address';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private empUrl = environment.empUrl;
  isEmpReview:boolean = false;
  constructor(private storageService: StorageService,
    private http: HttpClient,
    private toastr: ToastrService,
    private router: Router) { }

    saveEmpDetails(empDetail: Employee): Observable<any> {
      let url =  this.empUrl +"/employee";
      return this.http.post(url, empDetail, { observe: "response" as "body" })
    }

    checkEmpDetails(empDetail: Employee): Observable<any> {
      let url =  this.empUrl +"/checkEmpRecord";
      return this.http.post(url, empDetail, { observe: "response" as "body" })
    }

    saveOrgDetails(orgDetail: Organization): Observable<any> {
      let url =  this.empUrl +"/organization";
      return this.http.post(url, orgDetail, { observe: "response" as "body" })
    }

     saveAddressDetails(addressDetail: Address): Observable<any> {
      let url =  this.empUrl +"/address";
      return this.http.post(url, addressDetail, { observe: "response" as "body" })
    }

    getEmpDetails(email: any): Observable<any> {
      let url =  this.empUrl +"/getEmployee?email";
      return this.http.get(`${url}=${email}`);
    }

    getAddDetails(email: any): Observable<any> {
      let url =  this.empUrl +"/getAddress?email";
      return this.http.get(`${url}=${email}`);
    }

    getOrgDetails(userId: any): Observable<any> {
      let url =  this.empUrl +"/getOrganization?userId";
      return this.http.get(`${url}=${userId}`);
    }

    getCountryList(): Observable<any> {
      let url =  this.empUrl +"/getCountry";
      return this.http.get(`${url}`);
    }

    getStateList(countryName: string): Observable<any> {
      let url =  this.empUrl +"/getStates?country";
      return this.http.get(`${url}=${countryName}`);
    }

    getCitiesList(stateName: string): Observable<any> {
      let url =  this.empUrl +"/getCities?state";
      return this.http.get(`${url}=${stateName}`);
    }

  }