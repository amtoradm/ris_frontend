import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangeUserPasswordComponent } from './change-user-password/change-user-password.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { EmployeeComponent } from './employee/employee.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginComponent } from './login/login.component';
import { ReportComponent } from './report/report.component';
import { ReviewComponent } from './review/review.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { VisaComponent } from './visa/visa.component';
import { AuthGuard } from './_guards/auth.guard';

const routes: Routes = [
  { 
    path: 'login', 
    component: LoginComponent 
  },
  { 
    path: 'forgot-password', 
    component: ForgetpasswordComponent 
  },
  { 
    path: 'change-user-password', 
    component: ChangeUserPasswordComponent 
  },
  { 
    path: 'homepage', 
    component: HomepageComponent,
    // canActivate: [AuthGuard]
  },
  { 
    path: 'user-profile', 
    component: UserprofileComponent,
    // canActivate: [AuthGuard]
  },
  { 
    path: 'report', 
    component: ReportComponent,
    // canActivate: [AuthGuard]
  },
  { 
    path: 'employee', 
    component: EmployeeComponent,
    // canActivate: [AuthGuard]
  },
  { 
    path: 'review', 
    component: ReviewComponent,
    // canActivate: [AuthGuard]
  },
  { 
    path: 'visa', 
    component: VisaComponent,
    // canActivate: [AuthGuard]
  },
  { 
    path: 'change-password', 
    component: ChangepasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
