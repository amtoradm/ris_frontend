import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Visa } from '../_classes/visa';
import { EmployeeService } from '../_services/employee.service';
import { IdleService } from '../_services/idle.service';
import { StorageService } from '../_services/storage.service';
import { VisaService } from '../_services/visa.service';

@Component({
  selector: 'app-visa',
  templateUrl: './visa.component.html',
  styleUrls: ['./visa.component.css']
})
export class VisaComponent implements OnInit {
  form!: FormGroup;
  submitted = false;
  loading = false;

  private visaDetail = new Visa();
  status: boolean | undefined;
    visa_isUSCitizen: string| undefined;
    visa_isNonUSCitizen: string| undefined;
    visa_isEmplawfulPermanentResident: string| undefined;
    visa_isAuthorizedToWork: string| undefined;
    visa_permAlienRegNo: string| undefined;
    visa_alienAuthToWorkRegNo: string| undefined;
    visa_alienAuthToWorkI94AdmiNo: string| undefined;
    visa_alienAuthToWorkForeignPassportNum: string| undefined;
    visa_alienAuthToWorkForeignPassportCountry: string| undefined;
    visa_authWorkExpDate: any| undefined;
    visa_docTitle: string| undefined;
    visa_docNumber: string| undefined;
    visa_docExpDate: any| undefined;
    visa_visaCategory: string| undefined;
    visa_visaEarliestRenewalDate: any| undefined;
    visa_visaLatestRenewalDate: any| undefined;
    visa_visaExpDate: any| undefined;

    userId: number | undefined;
    user_role:  string| undefined;
    isVisaInfo:boolean = false;
    isDocumentsInfo:boolean = false;
    isDocumentsUpload:boolean = false;
    isEmpReview:boolean = false;


   constructor(private router: Router, 
    private visaService: VisaService,
    private idleService: IdleService,
    private empService: EmployeeService,
    private storageService: StorageService,
    private formBuilder: FormBuilder,
	private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.isVisaInfo = true;
    this.isEmpReview = this.empService.isEmpReview;
    this.userId = this.storageService.secureStorage.getItem("userId");
    this.user_role = this.storageService.secureStorage.getItem("role");
    
    this.visaService.getVisaDetails(this.userId).subscribe(
      (response) => {


        this.visa_isUSCitizen = response.isUSCitizen;
        this.visa_isNonUSCitizen = response.isNonUSCitizen;
        this.visa_isEmplawfulPermanentResident = response.isEmplawfulPermanentResident;
        this.visa_isAuthorizedToWork = response.isAuthorizedToWork;
        this.visa_permAlienRegNo = response.permAlienRegNo;
        this.visa_alienAuthToWorkRegNo = response.alienAuthToWorkRegNo;
        this.visa_alienAuthToWorkI94AdmiNo = response.alienAuthToWorkI94AdmiNo;
        this.visa_alienAuthToWorkForeignPassportNum = response.alienAuthToWorkForeignPassportNum;
        this.visa_alienAuthToWorkForeignPassportCountry = response.alienAuthToWorkForeignPassportCountry;
        this.visa_authWorkExpDate = this.datePipe.transform(response.authWorkExpDate,'yyyy-MM-dd');
        this.visa_docTitle = response.docTitle;
        this.visa_docNumber = response.docNumber;
        this.visa_docExpDate = this.datePipe.transform(response.docExpDate,'yyyy-MM-dd');
        this.visa_visaCategory= response.visaCategory;
		this.visa_visaEarliestRenewalDate= this.datePipe.transform(response.visaEarliestRenewalDate,'yyyy-MM-dd');
        this.visa_visaLatestRenewalDate= this.datePipe.transform(response.visaLatestRenewalDate,'yyyy-MM-dd');
        this.visa_visaExpDate= this.datePipe.transform(response.visaExpDate,'yyyy-MM-dd');
      },
      (error) => {
         if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
         
        }
      }
    );

    this.form = this.formBuilder.group(
      {
        isUSCitizen:                          ['', Validators.required],
        isNonUSCitizen:                       ['', Validators.required],
        isEmplawfulPermanentResident:         ['', Validators.required],
        isAuthorizedToWork:                   ['', Validators.required],
        permAlienRegNo:                       ['', Validators.required],
        alienAuthToWorkRegNo:                 ['', Validators.required],
        alienAuthToWorkI94AdmiNo:             ['', Validators.required],
        alienAuthToWorkForeignPassportNum:    ['', Validators.required],
        alienAuthToWorkForeignPassportCountry:['', Validators.required],
        authWorkExpDate:                      ['', Validators.nullValidator],
        docTitle:                             ['', Validators.required],
        docNumber:                            ['', Validators.required],
        docExpDate:                           ['', Validators.required],
        visaCategory:                         ['', Validators.required],
        visaEarliestRenewalDate:              ['', Validators.nullValidator], 
        visaLatestRenewalDate :               ['', Validators.nullValidator], 
        visaExpDate :                         ['', Validators.required], 
      }
    );
  }

  onSubmit(){
 
    this.submitted = true;
    if (this.form?.invalid) {
      return;
    }
    this.loading = true;
    this.visaDetail.userId = this.storageService.secureStorage.getItem("userId");
    this.visaDetail.isUSCitizen = this.isUSCitizen?.value;
    this.visaDetail.isNonUSCitizen = this.isNonUSCitizen?.value;
    this.visaDetail.isEmplawfulPermanentResident = this.isEmplawfulPermanentResident?.value;
    this.visaDetail.isAuthorizedToWork = this.isAuthorizedToWork?.value;
    this.visaDetail.permAlienRegNo = this.permAlienRegNo?.value;
    this.visaDetail.alienAuthToWorkRegNo = this.alienAuthToWorkRegNo?.value;
    this.visaDetail.alienAuthToWorkI94AdmiNo = this.alienAuthToWorkI94AdmiNo?.value;
    this.visaDetail.alienAuthToWorkForeignPassportNum = this.alienAuthToWorkForeignPassportNum?.value;
    this.visaDetail.alienAuthToWorkForeignPassportCountry = this.alienAuthToWorkForeignPassportCountry?.value;
    this.visaDetail.authWorkExpDate = this.authWorkExpDate?.value;
    this.visaDetail.docTitle = this.docTitle?.value;
	  this.visaDetail.docNumber = this.docNumber?.value;
	  this.visaDetail.docExpDate = this.docExpDate?.value;
    this.visaDetail.visaCategory            = this.visaCategory?.value;
    this.visaDetail.visaEarliestRenewalDate = this.visaEarliestRenewalDate?.value;
    this.visaDetail.visaLatestRenewalDate   = this.visaLatestRenewalDate?.value;
    this.visaDetail.visaExpDate             = this.visaExpDate?.value;
    this.visaService.saveVisaDetails(this.visaDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        this.status = false;
        alert(
          response.body.message
        );
        this.loading = false;
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          alert(
            "Visa Infomation saved UnSuccessfully!"
          );
          this.loading = false;
        }
      }
    );
   
  }

  onReset() {
    this.submitted = false;
    this.form?.reset();
  }

  onVisaInfo(){
    this.isDocumentsInfo = false;
    this.isDocumentsUpload = false;
    this.isVisaInfo = true;
  }

  onDocumentsInfo(){
    this.isDocumentsInfo = true;
    this.isDocumentsUpload = false;
    this.isVisaInfo = false;
  }

  onDocumentsUpload(){
    this.isDocumentsInfo = false;
    this.isDocumentsUpload = true;
    this.isVisaInfo = false;
  }

  get isUSCitizen() {
    return this.form.get("isUSCitizen");
  }

  get isNonUSCitizen() {
    return this.form.get("isNonUSCitizen");
  }

  get isEmplawfulPermanentResident() {
    return this.form.get("isEmplawfulPermanentResident");
  }

  get isAuthorizedToWork() {
    return this.form.get("isAuthorizedToWork");
  }

  get permAlienRegNo() {
    return this.form.get("permAlienRegNo");
  }

  get alienAuthToWorkRegNo() {
    return this.form.get("alienAuthToWorkRegNo");
  }
  get alienAuthToWorkI94AdmiNo() {
    return this.form.get("alienAuthToWorkI94AdmiNo");
  }
  get alienAuthToWorkForeignPassportNum() {
    return this.form.get("alienAuthToWorkForeignPassportNum");
  }
  get alienAuthToWorkForeignPassportCountry() {
    return this.form.get("alienAuthToWorkForeignPassportCountry");
  }
  get authWorkExpDate() {
    return this.form.get("authWorkExpDate");
  }
  get docTitle() {
    return this.form.get("docTitle");
  }
  get docNumber() {
    return this.form.get("docNumber");
  }
  get docExpDate() {
    return this.form.get("docExpDate");
  }
  get visaCategory() {
    return this.form.get("visaCategory");
  }
  
    get visaEarliestRenewalDate() {
    return this.form.get("visaEarliestRenewalDate");
  }
  
    get visaLatestRenewalDate() {
    return this.form.get("visaLatestRenewalDate");
  }
  
    get visaExpDate() {
    return this.form.get("visaExpDate");
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form?.controls;
  }

}