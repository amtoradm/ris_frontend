import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Employee } from '../_classes/employee';
import { EmployeeService } from '../_services/employee.service';
import { IdleService } from '../_services/idle.service';
import { StorageService } from '../_services/storage.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  form!: FormGroup;
  submitted = false;
  private empDetail = new Employee();
  status: boolean | undefined;
  first_name!: string;
  last_name!: string;
  email_id!: string;
  user_id!: number;
  user_role!: string;
  loading = false;

  isEmployeeInfo:boolean = false;
  isAddressInfo:boolean = false;
  isEmpReview:boolean = false;
  
  emp_middleInitial: string | undefined;
  emp_otherLastName: string | undefined;
  emp_dateOfBirth: any | undefined;
  emp_gender: string | undefined;
  emp_ssn: number | undefined;
  emp_telephoneNumber: number | undefined;
  emp_mobileNumber: number | undefined;
  emp_alternateMobileNumber: number | undefined;

  constructor(private router: Router, 
    private empService: EmployeeService,
    private idleService: IdleService,
    private storageService: StorageService,
    private formBuilder: FormBuilder,
	private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.isEmployeeInfo = true;
    this.isEmpReview = this.empService.isEmpReview;
    this.first_name = this.storageService.secureStorage.getItem("firstname")
    this.last_name = this.storageService.secureStorage.getItem("lastname");
    this.email_id = this.storageService.secureStorage.getItem("email");
    this.user_id = this.storageService.secureStorage.getItem("userId");
  
    this.empDetail.userId = this.user_id;
    this.empDetail.firstName = this.first_name;
    this.empDetail.lastName = this.last_name;
    this.empDetail.email = this.email_id;
    
    this.empService.checkEmpDetails(this.empDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        this.status = false;
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
         
        }
      }
    );

      this.empService.getEmpDetails(this.user_id).subscribe(
        (response) => {
          this.emp_middleInitial = response.middleInitial;
          this.emp_otherLastName = response.otherLastName;
          this.emp_dateOfBirth = this.datePipe.transform(response.dateOfBirth,'yyyy-MM-dd');
          this.emp_gender = response.gender;
          this.emp_ssn = response.ssn;
          this.emp_telephoneNumber = response.telephoneNumber;
          this.emp_mobileNumber = response.mobileNumber;
          this.emp_alternateMobileNumber = response.alternateMobileNumber;
        },
        (error) => {
           if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
            }
        }
      );

    this.form = this.formBuilder.group(
      {
        lastName:     ['', Validators.required],
        firstName:    ['', Validators.required],
        middleName:   ['', Validators.nullValidator],
        dateofBirth:  ['', Validators.required],
        gender:       ['', Validators.required],
        ssn:          ['', Validators.required],
        otherlastName:['', Validators.nullValidator],
        email:        ['', [Validators.required, Validators.email]],
        alternateMobileNumber:['', Validators.nullValidator],
        telephoneNumber:      ['',[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
        mobileNumber:         ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]]
      }
    );
    this.form.controls['lastName'].disable(); 
    this.form.controls['firstName'].disable(); 
    this.form.controls['email'].disable(); 
  }



  onSubmit(){
    this.submitted = true;
    if (this.form?.invalid) {
      return;
    }
    this.loading = true;
    this.empDetail.userId = this.user_id;
    this.empDetail.firstName = this.firstName?.value;
    this.empDetail.lastName = this.lastName?.value;
    this.empDetail.email = this.email?.value;
    this.empDetail.middleInitial = this.middleInitial?.value;
    this.empDetail.otherLastName = this.otherLastName?.value;
    this.empDetail.dateOfBirth = this.dateOfBirth?.value;
    this.empDetail.gender = this.gender?.value;
    this.empDetail.ssn = this.ssn?.value;
    this.empDetail.telephoneNumber = this.telephoneNumber?.value;
    this.empDetail.mobileNumber = this.mobileNumber?.value;
    this.empDetail.alternateMobileNumber = this.alternateMobileNumber?.value;
    this.empService.saveEmpDetails(this.empDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        this.status = false;
        alert(
          response.body.message
        );
        this.loading = false;
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          alert(
            "Employee Data Not Saved Successfully"
          );
          this.loading = false;
        }
      }
    );
   
  }

  onReset() {
    this.submitted = false;
    this.form?.reset();
  }

  nextToAddress(){
    
  }

  onEmployeeInfo(){
    this.isEmployeeInfo = true;
    this.isAddressInfo = false;
  }

  onAddressInfo(){
    this.isEmployeeInfo = false;
    this.isAddressInfo = true;
  }

  get firstName() {
    return this.form.get("firstName");
  }

  get lastName() {
    return this.form.get("lastName");
  }

  get email() {
    return this.form.get("email");
  }

  get middleInitial() {
    return this.form.get("middleName");
  }

  get otherLastName() {
    return this.form.get("otherlastName");
  }

  get dateOfBirth() {
    return this.form.get("dateofBirth");
  }
  get gender() {
    return this.form.get("gender");
  }
  get ssn() {
    return this.form.get("ssn");
  }
  get telephoneNumber() {
    return this.form.get("telephoneNumber");
  }
  get mobileNumber() {
    return this.form.get("mobileNumber");
  }
  get alternateMobileNumber() {
    return this.form.get("alternateMobileNumber");
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form?.controls;
  }

}
