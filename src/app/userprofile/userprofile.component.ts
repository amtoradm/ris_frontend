﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../_services/alert.service';
import Validation from '../_utils/validation';
import { User } from '../_classes/user';
import { UserService } from '../_services/user.service';
import { IdleService } from '../_services/idle.service';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {
  private userDetail = new User();
  form!: FormGroup;
  loading = false;
  submitted = false;
  status!: boolean;

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private userService: UserService,
      private idleService: IdleService,
      private router: Router,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.form = this.formBuilder.group(
        {
          lastName:       ['', [Validators.required, 
            Validators.pattern("^[A-Za-z -]+$") 
          ]],
          firstName:      ['', [Validators.required, 
            Validators.pattern("^[A-Za-z -]+$") 
          ]],
          email:          ['', [Validators.required, Validators.email]],
          password:       ['', [Validators.required, 
            Validators.minLength(8), 
            Validators.maxLength(40),
            Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
          ]],
          confirmPassword:['', Validators.required],
          roles:          ['', Validators.required],
        },
        {
          validators: [Validation.match('password', 'confirmPassword')]
        }
      );
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  onSubmit() {
      this.submitted = true;

      // reset alerts on submit
      this.alertService.clear();

      // stop here if form is invalid
      if (this.form.invalid) {
          return;
      }
      this.loading = true;
      console.log(JSON.stringify(this.form?.value, null, 2));
    this.userDetail.firstName = this.firstName?.value;
    this.userDetail.lastName = this.lastName?.value;
    this.userDetail.email = this.email?.value;
    this.userDetail.password = this.password?.value;
    this.userDetail.roles = this.roles?.value;
    this.userService.register(this.userDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        this.status = false;
        this.loading = false;
        alert(
          response.body.message
        );
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          alert(
            "User registration UnSuccessfully!"
          );
        }
      }
    );
  }

  onReset() {
    this.submitted = false;
    this.form?.reset();
  }

  get firstName() {
    return this.form.get("firstName");
  }

  get lastName() {
    return this.form.get("lastName");
  }

  get email() {
    return this.form.get("email");
  }

  get password() {
    return this.form.get("password");
  }

  get roles() {
    return this.form.get("roles");
  }
}
