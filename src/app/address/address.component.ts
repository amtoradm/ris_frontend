import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Address } from '../_classes/address';
import { EmployeeService } from '../_services/employee.service';
import { IdleService } from '../_services/idle.service';
import { StorageService } from '../_services/storage.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {
  form!: FormGroup;
  submitted = false;
  private addressDetail = new Address();
  status!: boolean;
  loading = false;

  address_aptNumber: string | undefined;
  address_sreetNumberAndName: string | undefined;
  address_pinCodeOrZipCode: number | undefined;
  address_areaLocality: string | undefined;
  address_townCity: string | undefined;
  address_state: string | undefined;
  address_country: string | undefined;
  userId: number | undefined;
  user_role: string | undefined;
  countryList: any;
  stateList: any;
  cityList: any;
  countryName :  string | undefined;
  stateName :  string | undefined;
  constructor(private router: Router, 
    private empService: EmployeeService,
    private storageService: StorageService,
    private idleService: IdleService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.userId = this.storageService.secureStorage.getItem("userId");
    this.user_role = this.storageService.secureStorage.getItem("role");
    this.empService.getCountryList().subscribe(
      (response) => {

        this.countryList = response;
      },
      (error) => {
         if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
         }
      }
    );
    this.empService.getAddDetails(this.userId).subscribe(
      (response) => {
        this.address_aptNumber = response.aptNumber;
        this.address_sreetNumberAndName = response.sreetNumberAndName;
        this.address_pinCodeOrZipCode = response.pinCodeOrZipCode;
        this.address_areaLocality = response.areaLocality;
        this.address_townCity = response.townCity;
        this.address_state = response.state;
        this.address_country = response.country;  
        
      },
      (error) => {
         if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
         }
      }
    );


     
       

    this.form = this.formBuilder.group(
      {
        aptNumber:          ['', Validators.required],
        sreetNumberAndName: ['', Validators.required],
        pinCodeOrZipCode:   ['', Validators.required],
        areaLocality:       ['', Validators.required],
        townCity:           ['', Validators.required],
        state:              ['', Validators.required],
        country:            ['', Validators.required]
        }
    );
  }

  onSubmit(){
    this.submitted = true;
    if (this.form?.invalid) {
      return;
    }
    this.loading = true;
    this.addressDetail.userId = this.storageService.secureStorage.getItem("userId");
    this.addressDetail.aptNumber = this.aptNumber?.value;
    this.addressDetail.sreetNumberAndName = this.sreetNumberAndName?.value;
    this.addressDetail.pinCodeOrZipCode = this.pinCodeOrZipCode?.value;
    this.addressDetail.areaLocality = this.areaLocality?.value;
    this.addressDetail.townCity = this.townCity?.value;
    this.addressDetail.state = this.state?.value;
    this.addressDetail.country = this.country?.value;

    this.empService.saveAddressDetails(this.addressDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        this.status = false;
        alert(
          response.body.message
        );
        this.loading = false;
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          alert(
            "Address Information not saved Successfully!"
          );
          this.loading = false;
        }
      }
    );
  }

  onChangeCountry(countryId: any) {
    this.countryName = countryId.target.value;
    if (this.countryName) {
      this.empService.getStateList(this.countryName).subscribe(
        (response) => { 
          this.stateList = response;
        },
        (error) => {
           if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
           }
        }
      );
    } else {
      this.stateList = null;
      this.cityList = null;
    }
  }

  onChangeState(stateId: any) {
    this.stateName = stateId.target.value;
    if (this.stateName) {
      this.empService.getCitiesList(this.stateName).subscribe(
        (response) => {
          this.cityList = response;
        },
        (error) => {
           if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
           }
        }
      );
    } else {
      this.cityList = null;
    }
  }

  onReset() {
    this.submitted = false;
    this.form?.reset();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form?.controls;
  }

  get aptNumber() {
    return this.form.get("aptNumber");
  }

  get sreetNumberAndName() {
    return this.form.get("sreetNumberAndName");
  }

  get pinCodeOrZipCode() {
    return this.form.get("pinCodeOrZipCode");
  }

  get areaLocality() {
    return this.form.get("areaLocality");
  }

  get townCity() {
    return this.form.get("townCity");
  }

  get state() {
    return this.form.get("state");
  }

  get country() {
    return this.form.get("country");
  }

}