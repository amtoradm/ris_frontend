import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService } from '../_services/alert.service';
import { AdminService } from '../_services/admin.service';
import Validation from '../_utils/validation';
import { PasswordModel } from '../_classes/password';
import { UserService } from '../_services/user.service';
import { IdleService } from '../_services/idle.service';

enum TokenStatus {
    Validating,
    Valid,
    Invalid
}

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  TokenStatus = TokenStatus;
  private passwordDetail = new PasswordModel();
  tokenStatus = TokenStatus.Validating;
  token!: any;
  form!: FormGroup;
  loading = false;
  submitted = false;
    status!: boolean;
   

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private accountService: AdminService,
      private userService: UserService,
      private idleService: IdleService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
      this.form = this.formBuilder.group({
        password:       ['', [Validators.required, 
          Validators.minLength(8), 
          Validators.maxLength(40),
          Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
        ]],
        confirmPassword:['', Validators.required]
      },
      {
          validators: [Validation.match('password', 'confirmPassword')]
        } 
      );

      const token = this.route.snapshot.queryParams['token'];
      this.token = token;

        this.userService.verifyToken(token).subscribe(
          (response) => {

            console.log(response);
            this.tokenStatus = TokenStatus.Valid;
          },
          (error) => {
            this.tokenStatus = TokenStatus.Invalid;

          });
      // remove token from url to prevent http referer leakage
      this.router.navigate([], { relativeTo: this.route, replaceUrl: true });

  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  onSubmit() {
      this.submitted = true;

      // reset alerts on submit
      this.alertService.clear();

      // stop here if form is invalid
      if (this.form.invalid) {
          return;
      }

      this.loading = true;
        this.passwordDetail.password = this.password?.value;
        this.passwordDetail.token = this.token;

      this.userService.resetPassword(this.passwordDetail).subscribe(
        (response) => {
          this.idleService.checkUserIdleOrNot();
          this.status = false;
          this.loading = false;
          alert(
            "Password Reset Successful !!"
          );
          this.router.navigate(["/login"]);
        },
        (error) => {
          this.status = false;
          if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
            alert(
              "Password has not Reset Successful !!"
            );
          }
        }
      );
  }

  onReset() {
    this.submitted = false;
    this.form?.reset();
  }

  get password() {
    return this.form.get("password");
  }

  get confirmPassword() {
    return this.form.get("confirmPassword");
  }
}