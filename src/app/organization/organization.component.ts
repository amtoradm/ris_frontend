import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Organization } from '../_classes/organization';
import { EmployeeService } from '../_services/employee.service';
import { IdleService } from '../_services/idle.service';
import { StorageService } from '../_services/storage.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit {
  form!: FormGroup;
  submitted = false;
  private orgDetail = new Organization();
  status!: boolean;
  loading = false;
  org_user_id: number | undefined;
  org_lastNameOfEmpOrAuthorizedRepresentative: string| undefined;
  org_firstNameOfEmpOrAuthorizedRepresentative: string| undefined;
  org_empBusinessOrOrgName: string| undefined;
  org_titleOfEmpOrAuthorizedRepresentative: string| undefined;
  org_firstDayOfEmployment: any| undefined;
  org_dateofRehire: any| undefined;
  org_streetNumberAndName: string | undefined;
  org_pinCodeOrZipCode: number | undefined;
  org_townCity: string | undefined;
  org_state: string | undefined;
  org_country: string | undefined;
  user_role: string | undefined;
  org_salary: string | undefined;
  org_clientName: string | undefined;
  org_employmentLocation: string | undefined;
  org_projectEndDate: any | undefined;
  countryList: any;
  stateList: any;
  cityList: any;
  countryName :  string | undefined;
  stateName :  string | undefined;

  constructor(private router: Router, 
    private empService: EmployeeService,
    private storageService: StorageService,
    private idleService: IdleService,
    private formBuilder: FormBuilder,
	private datePipe: DatePipe) { }

  ngOnInit(): void {

    this.user_role = this.storageService.secureStorage.getItem("role");

    this.empService.getCountryList().subscribe(
      (response) => {

        this.countryList = response;
      },
      (error) => {
         if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
         }
      }
    );

    
    this.form = this.formBuilder.group(
      {
        lastNameOfEmpOrAuthorizedRepresentative:  ['', Validators.required],
        firstNameOfEmpOrAuthorizedRepresentative: ['', Validators.required],
        empBusinessOrOrgName:                     ['', Validators.required],
        titleOfEmpOrAuthorizedRepresentative:     ['', Validators.required],
        firstDayOfEmployment:                     ['', Validators.required],
        dateofRehire:                             ['', Validators.nullValidator],
        streetNumberAndName:                       ['', Validators.required],
        pinCodeOrZipCode:                         ['', Validators.required],
        townCity:                                 ['', Validators.required],
        country:                                  ['', Validators.required],
        state:                                    ['', Validators.required],
        salary: 									['', Validators.required],
        clientName: 								['', Validators.required],
        employmentLocation: 						['', Validators.required],
        projectEndDate: 							['', Validators.nullValidator]
        }
    );
  }

  onSubmit(){
    this.submitted = true;
    if (this.form?.invalid) {
      return;
    } 
    this.loading = true;
    console.log(JSON.stringify(this.form?.value, null, 2));
    this.orgDetail.userId = this.storageService.secureStorage.getItem("org_user_id");
    this.orgDetail.lastNameOfEmpOrAuthorizedRepresentative = this.lastNameOfEmpOrAuthorizedRepresentative?.value;
    this.orgDetail.firstNameOfEmpOrAuthorizedRepresentative = this.firstNameOfEmpOrAuthorizedRepresentative?.value;
    this.orgDetail.empBusinessOrOrgName = this.empBusinessOrOrgName?.value;
    this.orgDetail.titleOfEmpOrAuthorizedRepresentative = this.titleOfEmpOrAuthorizedRepresentative?.value;
    this.orgDetail.firstDayOfEmployment = this.firstDayOfEmployment?.value;
    this.orgDetail.dateofRehire = this.dateofRehire?.value;
    this.orgDetail.streetNumberAndName = this.streetNumberAndName?.value;
    this.orgDetail.pinCodeOrZipCode = this.pinCodeOrZipCode?.value;
    this.orgDetail.townCity = this.townCity?.value;
    this.orgDetail.state = this.state?.value;
    this.orgDetail.country = this.country?.value;
    this.orgDetail.salary		 = this.salary?.value;	
    this.orgDetail.clientName		 = this.clientName?.value;
    this.orgDetail.employmentLocation = this.employmentLocation?.value;
    this.orgDetail.projectEndDate	 = this.projectEndDate?.value;
    this.empService.saveOrgDetails(this.orgDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        this.status = false;
        alert(
          response.body.message
        );
        this.loading = false;
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          alert(
            "Organization Information not saved Successfully!"
          );
          this.loading = false;
        }
      }
    );
   
  }

  reload(){
    this.org_user_id = this.storageService.secureStorage.getItem("org_user_id")
    this.empService.getOrgDetails(this.org_user_id).subscribe(
      (response) => {
        console.log(response)
        this.org_lastNameOfEmpOrAuthorizedRepresentative =response.lastNameOfEmpOrAuthorizedRepresentative;
        this.org_firstNameOfEmpOrAuthorizedRepresentative =response.firstNameOfEmpOrAuthorizedRepresentative;
        this.org_empBusinessOrOrgName =response.empBusinessOrOrgName;
        this.org_titleOfEmpOrAuthorizedRepresentative =response.titleOfEmpOrAuthorizedRepresentative;
        this.org_firstDayOfEmployment = this.datePipe.transform(response.firstDayOfEmployment,'yyyy-MM-dd');
        this.org_dateofRehire = this.datePipe.transform(response.dateofRehire,'yyyy-MM-dd');
        this.org_streetNumberAndName =response.streetNumberAndName;
        this.org_pinCodeOrZipCode =response.pinCodeOrZipCode;
        this.org_townCity =response.townCity;
        this.org_state =response.state;
        this.org_country =response.country;
        this.org_salary =response.salary;
        this.org_clientName =response.clientName;
        this.org_employmentLocation =response.employmentLocation;
        this.org_projectEndDate =this.datePipe.transform(response.projectEndDate,'yyyy-MM-dd');
      },
      (error) => {
         if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
       
        }
      }
    );
  }

  onChangeCountry(countryId: any) {
    this.countryName = countryId.target.value;
    if (this.countryName) {
      this.empService.getStateList(this.countryName).subscribe(
        (response) => { 
          this.stateList = response;
        },
        (error) => {
           if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
           }
        }
      );
    } else {
      this.stateList = null;
      this.cityList = null;
    }
  }

  onChangeState(stateId: any) {
    this.stateName = stateId.target.value;
    if (this.stateName) {
      this.empService.getCitiesList(this.stateName).subscribe(
        (response) => {
          this.cityList = response;
          console.log(this.cityList);
        },
        (error) => {
           if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
           }
        }
      );
    } else {
      this.cityList = null;
    }
  }

  onReset() {
    this.submitted = false;
    this.form?.reset();
  }

  get lastNameOfEmpOrAuthorizedRepresentative() {
    return this.form.get("lastNameOfEmpOrAuthorizedRepresentative");
  }

  get firstNameOfEmpOrAuthorizedRepresentative() {
    return this.form.get("firstNameOfEmpOrAuthorizedRepresentative");
  }

  get empBusinessOrOrgName() {
    return this.form.get("empBusinessOrOrgName");
  }

  get titleOfEmpOrAuthorizedRepresentative() {
    return this.form.get("titleOfEmpOrAuthorizedRepresentative");
  }

  get firstDayOfEmployment() {
    return this.form.get("firstDayOfEmployment");
  }

  get dateofRehire() {
    return this.form.get("dateofRehire");
  }

  get streetNumberAndName() {
    return this.form.get("streetNumberAndName");
  }
  get pinCodeOrZipCode() {
    return this.form.get("pinCodeOrZipCode");
  }
  get townCity() {
    return this.form.get("townCity");
  }

  get state() {
    return this.form.get("state");
  }
  get country() {
    return this.form.get("country");
  }

  get salary() {
    return this.form.get("salary");
  }
  
    get clientName() {
    return this.form.get("clientName");
  }
  
    get employmentLocation() {
    return this.form.get("employmentLocation");
  }
  
    get projectEndDate() {
    return this.form.get("projectEndDate");
  }
  
  get f(): { [key: string]: AbstractControl } {
    return this.form?.controls;
  }

}
