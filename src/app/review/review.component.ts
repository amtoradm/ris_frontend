import { Component, OnInit } from '@angular/core';
import { Address } from '../_classes/address';
import { Documents } from '../_classes/documents';
import { Employee } from '../_classes/employee';
import { Organization } from '../_classes/organization';
import { Visa } from '../_classes/visa';
import { EmployeeService } from '../_services/employee.service';
import { StorageService } from '../_services/storage.service';
import { VisaService } from '../_services/visa.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
  userId: string | undefined;
  lastName: string | undefined;
  firstName: string | undefined;
  middleInitial: string | undefined;
  otherLastName: string | undefined;
  dateOfBirth: Date |undefined;
  gender: string | undefined;
  USSocSecNum: string | undefined;
  email: string | undefined;
  telephoneNumber: number | undefined;
  mobileNumber: number | undefined;
  alternateMobileNumber: number | undefined;

  aptNumber: string | undefined;
  sreetNumberAndName: string | undefined;
  pinCodeOrZipCode: number | undefined;
  areaLocality: string | undefined;
  townCity: string | undefined;
  state: string | undefined;
  country: string | undefined;

  lastNameOfEmpOrAuthorizedRepresentative: string| undefined;
  firstNameOfEmpOrAuthorizedRepresentative: string| undefined;
  empBusinessOrOrgName: string| undefined;
  titleOfEmpOrAuthorizedRepresentative: string| undefined;
  firstDayOfEmployment: Date| undefined;
  dateofRehire: Date| undefined;
  org_streetNumberAndName: string | undefined;
  org_pinCodeOrZipCode: number | undefined;
  org_townCity: string | undefined;
  org_state: string | undefined;
  org_country: string | undefined;
  org_salary: string | undefined;
	org_clientName: string | undefined;
	org_employmentLocation: string | undefined;
	org_projectEndDate: Date | undefined;
 
    isUSCitizen: string| undefined;
    isNonUSCitizen: string| undefined;
    isEmplawfulPermanentResident: string| undefined;
    isAuthorizedToWork: string| undefined;
    permAlienRegNo: string| undefined;
    alienAuthToWorkRegNo: string| undefined;
    alienAuthToWorkI94AdmiNo: string| undefined;
    alienAuthToWorkForeignPassportNum: string| undefined;
    alienAuthToWorkForeignPassportCountry: string| undefined;
    authWorkExpDate: Date| undefined;
    docTitle: string| undefined;
    docNumber: string| undefined;
    docExpDate: string| undefined;
    visa_visaCategory: string| undefined;
    visa_visaEarliestRenewalDate: Date| undefined;
    visa_visaLatestRenewalDate: Date| undefined;
    visa_visaExpDate: Date| undefined;

    documentTitle: string| undefined;
    issuingAuthority: string| undefined;
    documentNumber: string| undefined;
    expDate: string| undefined;
    additionalInfo: string| undefined;

  private employee = new Employee();
  private address = new Address();
  private organization = new Organization();
  private visa = new Visa();
  private document = new Documents();
  user_id: any;
  user_role: string | undefined;

  constructor(   private empService: EmployeeService,
    private storageService: StorageService,
    private visaService: VisaService) { }

   
  empStatus:boolean = true;
  isEditEmpInfo:boolean = false;
  isEditAddInfo:boolean = false;
  isEditOrgInfo:boolean = false;
  isEditVisaInfo:boolean = false;
  isEditDocInfo:boolean = false;

  ngOnInit(): void {

    this.empService.isEmpReview=true;
    this.user_id = this.storageService.secureStorage.getItem("userId");
    this.user_role = this.storageService.secureStorage.getItem("role");
      this.userId = this.user_id;
      this.empService.getEmpDetails(this.user_id).subscribe(
        (response) => {
          this.employee = response;  
          this.lastName = response.lastName;
          this.firstName = response.firstName;
          this.middleInitial = response.middleInitial;
          this.otherLastName = response.otherLastName;
          this.dateOfBirth = response.dateOfBirth;
          this.gender = response.gender;
          this.USSocSecNum = response.ssn;
          this.email = response.email;
          this.telephoneNumber = response.telephoneNumber;
          this.mobileNumber = response.mobileNumber;
          this.alternateMobileNumber = response.alternateMobileNumber;
        },
        (error) => {
           if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
            
          }
        }
      );
      
      this.empService.getAddDetails(this.userId).subscribe(
        (response) => {
          this.address = response; 
          this.aptNumber = response.aptNumber;
          this.sreetNumberAndName = response.sreetNumberAndName;
          this.pinCodeOrZipCode = response.pinCodeOrZipCode;
          this.areaLocality = response.areaLocality;
          this.townCity = response.townCity;
          this.state = response.state;
          this.country = response.country;  
        },
        (error) => {
           if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
           
          }
        }
      );
     
      this.empService.getOrgDetails(this.userId).subscribe(
        (response) => {
          this.organization = response;   
          this.lastNameOfEmpOrAuthorizedRepresentative =response.lastNameOfEmpOrAuthorizedRepresentative;
          this.firstNameOfEmpOrAuthorizedRepresentative =response.firstNameOfEmpOrAuthorizedRepresentative;
          this.empBusinessOrOrgName =response.empBusinessOrOrgName;
          this.titleOfEmpOrAuthorizedRepresentative =response.titleOfEmpOrAuthorizedRepresentative;
          this.firstDayOfEmployment =response.firstDayOfEmployment;
          this.dateofRehire =response.dateofRehire;
          this.org_streetNumberAndName =response.streetNumberAndName;
          this.org_pinCodeOrZipCode =response.pinCodeOrZipCode;
          this.org_townCity =response.townCity;
          this.org_state =response.state;
          this.org_country =response.country;
          this.org_salary =response.salary;
          this.org_clientName =response.clientName;
          this.org_employmentLocation =response.employmentLocation;
          this.org_projectEndDate =response.projectEndDate;
        },
        (error) => {
           if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
           
          }
        }
      );

      this.visaService.getVisaDetails(this.userId).subscribe(
        (response) => {
          console.log(response)
          this.visa = response;
          this.isUSCitizen = response.isUSCitizen;
          this.isNonUSCitizen = response.isNonUSCitizen;
          this.isEmplawfulPermanentResident = response.isEmplawfulPermanentResident;
          this.isAuthorizedToWork = response.isAuthorizedToWork;
          this.permAlienRegNo = response.permAlienRegNo;
          this.alienAuthToWorkRegNo = response.alienAuthToWorkRegNo;
          this.alienAuthToWorkI94AdmiNo = response.alienAuthToWorkI94AdmiNo;
          this.alienAuthToWorkForeignPassportNum = response.alienAuthToWorkForeignPassportNum;
          this.alienAuthToWorkForeignPassportCountry = response.alienAuthToWorkForeignPassportCountry;
          this.authWorkExpDate = response.authWorkExpDate;
          this.visa_visaCategory           = response.visaCategory;
          this.visa_visaEarliestRenewalDate= response.visaEarliestRenewalDate;
          this.visa_visaLatestRenewalDate  = response.visaLatestRenewalDate;
          this.visa_visaExpDate            = response.visaExpDate;
          this.docTitle = response.docTitle;
          this.docNumber = response.docNumber;
          this.docExpDate = response.docExpDate;
        },
        (error) => {
           if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
           
          }
        }
      );
      

      this.visaService.getDocDetails(this.userId).subscribe(
        (response) => {
          console.log(response)
          this.document = response;   
          this.documentTitle = response.documentTitle;
          this.issuingAuthority = response.issuingAuthority;
          this.documentNumber = response.documentNumber;
          this.expDate = response.expDate;
          this.additionalInfo = response.additionalInfo;
        },
        (error) => {
           if (error.status == 403) {
            console.log("Error = 403");
          } else {
            console.log("Error");
            
          }
        }
      );
  }

  editInfo(){
    this.isEditEmpInfo = true;
    console.log(this.isEditEmpInfo);
  }

  saveEmp(){
    this.isEditEmpInfo = true;
    this.isEditAddInfo = false;
    this.isEditOrgInfo = false;
    this.isEditVisaInfo = false;
    this.isEditDocInfo = false;
  }
  saveAdd(){
    this.isEditEmpInfo = false;
    this.isEditAddInfo = true;
    this.isEditOrgInfo = false;
    this.isEditVisaInfo = false;
    this.isEditDocInfo = false;

  }
  saveOrg(){
    this.isEditEmpInfo = false;
    this.isEditAddInfo = false;
    this.isEditOrgInfo = true;
    this.isEditVisaInfo = false;
    this.isEditDocInfo = false;
  }
  saveVisa(){
    this.isEditEmpInfo = false;
    this.isEditAddInfo = false;
    this.isEditOrgInfo = false;
    this.isEditVisaInfo = true;
    this.isEditDocInfo = false;
  }
  saveDoc(){
    this.isEditEmpInfo = false;
    this.isEditAddInfo = false;
    this.isEditOrgInfo = false;
    this.isEditVisaInfo = false;
    this.isEditDocInfo = true;
  }
 
}
