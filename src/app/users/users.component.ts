import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../_classes/user';
import { IdleService } from '../_services/idle.service';
import { StorageService } from '../_services/storage.service';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  private userDetail = new User();
  status!: boolean;
  listOfUser: any;
  searchedKeyword!: string;

  constructor(private router: Router, 
    private idleService: IdleService,
    private storageService: StorageService,
    private userService: UserService
    ) { }

  ngOnInit(): void {
    this.listOfUsers();
  }

  listOfUsers(){
   
    this.userService.getUsersList().subscribe(
      (response) => {  
        this.listOfUser = response;
        console.log( this.listOfUser);
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          alert(
            "User List Not Found"
          );
        }
      }
    );
   
  }

  deleteUser(email_id:any){ 
    if(confirm("Are you sure you want to delete "+" ' "+ email_id + " ' ")) {
   
    this.userDetail.email = email_id;
 
    this.userService.deleteUser(this.userDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        alert(
          response.body.message
        );
        this.listOfUsers();
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          console.log("Error = 403");
        } else {
          console.log("Error");
          alert(
            "User Not Deleted!"
          );
        }
      }
    );
  }
}

enableAccount(enabled:any,  email_id:any){
  if(enabled){
  if(confirm("Are you sure you want to Inactive account ")){
  this.userDetail.email = email_id;
  this.userDetail.enabled = enabled;
 
  this.userService.EnableOrisableUser(this.userDetail).subscribe(
    (response) => {
      this.idleService.checkUserIdleOrNot();
      alert(
        response.body.message
      );
      this.listOfUsers();
    },
    (error) => {
      this.status = false;
      if (error.status == 403) {
        console.log("Error = 403");
      } else {
        console.log("Error");
        alert(
          "User Not Enabled or Disabled!"
        );
      }
    }
  );
}
  
}
if(!enabled){
  if(confirm("Are you sure you want to Active account ")){
  this.userDetail.email = email_id;
  this.userDetail.enabled = enabled;
 
  this.userService.EnableOrisableUser(this.userDetail).subscribe(
    (response) => {
      this.idleService.checkUserIdleOrNot();
      alert(
        response.body.message
      );
      this.listOfUsers();
    },
    (error) => {
      this.status = false;
      if (error.status == 403) {
        console.log("Error = 403");
      } else {
        console.log("Error");
        alert(
          "User Not Enabled or Disabled!"
        );
      }
    }
  );
}
} 

}

fillOrganization(org_user_id:any){
  this.storageService.secureStorage.setItem("org_user_id", org_user_id);
}

}
