﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AdminDetail } from "../_classes/admin-details";
import { AdminService } from '../_services/admin.service';
import { IdleService } from '../_services/idle.service';
import { StorageService } from '../_services/storage.service';
import { AlertService } from '../_services/alert.service';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private adminDetail = new AdminDetail();
    form!: FormGroup;
    loading = false;
    submitted = false;
    status: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private idleService: IdleService,
    private storageService: StorageService,
    private adminService: AdminService,
    private alertService: AlertService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email:    ['', Validators.required],
      password: ['', Validators.required]
  });
  }

  // convenience getter for easy access to form fields
  get f(): { [key: string]: AbstractControl } {
    return this.form?.controls;
  }

  checkActiveAccount(){
    this.submitted = true;
    this.alertService.clear();

    //stop here if form is invalid
    if (this.form?.invalid) {
        return;
    }
   
    this.loading = true;
    this.adminDetail.email = this.email?.value;
    this.adminDetail.password = this.password?.value;
    this.status = true;

    this.adminService.login(this.adminDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        let result = response.body.token;
        console.log(response);
        this.storageService.secureStorage.setItem("userId",  response.body.id);
        this.storageService.secureStorage.setItem("email",  response.body.email);
        this.storageService.secureStorage.setItem("firstname",  response.body.username);
        this.storageService.secureStorage.setItem("lastname",  response.body.lastname);
        this.storageService.secureStorage.setItem("role",  response.body.roles);
        this.authService.isAuthenticate = true;
        this.status = false;
        if (result!==null) {
          this.storageService.secureStorage.setItem("token", result);
          this.storageService.secureStorage.setItem("userName",  response.body.username);
          this.storageService.secureStorage.setItem("roleId",  response.body.id);
          this.router.navigate(["/homepage"]);
        } else {
          alert(
            "please register before login Or Invalid combination of Email and password"
          );
        }
      },
      (error) => {
        this.status = false;
        this.alertService.error("Username or password is incorrect");
        this.loading = false;
        alert(
          "Password is incorrect"
        );
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form?.invalid) {
      return;
  }
  
    this.adminService.isActiveAccount(this.email?.value).subscribe(
      (response) => {
          console.log(response);
          if(response){
            this.checkActiveAccount();          
          }else{
            alert(
              "User is not active , Please contact your Administrator !!"
            );
          }
          console.log("isEnabled = "+response.enabled);
      },
      (error) => {
        this.loading = false;
        alert(
          "User account is not exist!!"
        );
      });
}

  get email() {
    return this.form.get("email");
  }

  get password() {
    return this.form.get("password");
  }
}
